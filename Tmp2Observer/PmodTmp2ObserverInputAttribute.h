#ifndef SFPMODTMP2OBSERVERSTATEATTRIBUTE_H
#define SFPMODTMP2OBSERVERSTATEATTRIBUTE_H

#include "MsgObserverInputAttribute_t.h"
#include "PmodTmp2.h"

namespace SF {

class PmodTmp2ObserverInputAttribute : public MsgObserverInputAttribute_t<float>
{
public:
    PmodTmp2ObserverInputAttribute(  const float& v
                                    ,const std::string& l
                                    ,MsgObserverObject& owner
                                    ,const size_t& maxsub
                                    ,const timespec& sp
                                    ,const uint32_t& rc
                                    ,PmodTmp2* h);
    virtual ~PmodTmp2ObserverInputAttribute();

protected:
    virtual bool read(float& t);

private:
    PmodTmp2ObserverInputAttribute(const PmodTmp2ObserverInputAttribute& orig);
    PmodTmp2ObserverInputAttribute& operator=(PmodTmp2ObserverInputAttribute& orig);

    PmodTmp2* mod;
};
}

#endif /* SFPMODTMP2OBSERVERSTATEATTRIBUTE_H */
