#ifndef SFPMODTMP2OBSERVEROBJECT_H
#define SFPMODTMP2OBSERVEROBJECT_H

#include "MsgObserverObject.h"
#include "PmodTmp2ObserverInputAttribute.h"

namespace SF {

class PmodTmp2ObserverObject : public MsgObserverObject
{
public:
    PmodTmp2ObserverObject(
            const uint8_t& adapter, const uint8_t& device
            ,const timespec& sp, const uint32_t& rc, const size_t& maxsub=0
            ,MsgObserverObject* owner=nullptr, const MO_Label& l="tmp2");
    virtual ~PmodTmp2ObserverObject();

    virtual bool getAllStatus(OL_Container& labels) override;

private:
    PmodTmp2ObserverObject(const PmodTmp2ObserverObject& orig);
    PmodTmp2ObserverObject& operator=(const PmodTmp2ObserverObject& orig);

    PmodTmp2 mod;

    PmodTmp2ObserverInputAttribute temperature;
};

}

#endif /* SFPMODTMP2OBSERVEROBJECT_H */
