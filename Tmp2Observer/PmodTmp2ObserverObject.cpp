#include "PmodTmp2ObserverObject.h"

SF::PmodTmp2ObserverObject::PmodTmp2ObserverObject(
        const uint8_t& adapter, const uint8_t& device
        ,const timespec& sp,const uint32_t& rc, const size_t& maxsub
        ,MsgObserverObject* owner, const MO_Label& l
)
    :MsgObserverObject(1, l, owner)
    ,mod(adapter, device)
    ,temperature(0, "temperature", *this, maxsub, sp, rc, &mod) {}

SF::PmodTmp2ObserverObject::~PmodTmp2ObserverObject() {}

bool SF::PmodTmp2ObserverObject::getAllStatus(OL_Container& labels)
{
    const bool ret = true;

    labels.clear();

    labels.push_back(temperature.getLabel());

    return ret;
}
