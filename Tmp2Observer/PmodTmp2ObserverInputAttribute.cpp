#include "PmodTmp2ObserverInputAttribute.h"

SF::PmodTmp2ObserverInputAttribute::PmodTmp2ObserverInputAttribute(
        const float& v
        ,const std::string& l
        ,MsgObserverObject& owner
        ,const size_t& maxsub
        ,const timespec& sp
        ,const uint32_t& rc
        ,PmodTmp2* h)
    :MsgObserverInputAttribute_t<float>(v, l, owner, maxsub, sp, rc)
    ,mod(h) {}

SF::PmodTmp2ObserverInputAttribute::~PmodTmp2ObserverInputAttribute(){}

bool SF::PmodTmp2ObserverInputAttribute::read(float& t)
{
    bool ret = false;

    if(mod!=nullptr)
    {
        if(mod->getTemperatureFloat(t))
        {
            ret = true;
        }
    }

    return ret;
}
