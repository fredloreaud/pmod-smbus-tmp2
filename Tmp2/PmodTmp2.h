#ifndef PMODTMP2_H
#define PMODTMP2_H

#include "SMBusDevice.h"

// https://reference.digilentinc.com/reference/pmod/pmodtmp2/reference-manual
// http://www.analog.com/en/products/analog-to-digital-converters/integrated-special-purpose-converters/digital-temperature-sensors/adt7420.html

namespace PMOD {

    const uint8_t PmodTmp2DeviceMin = 0x48;  ///< Pmod TMP2 supports only device numbers in [0x48-0x4b]
    const uint8_t PmodTmp2DeviceMax = 0x4b;  ///< Pmod TMP2 supports only device numbers in [0x48-0x4b]

class PmodTmp2  ///< Diligent Pmod TMP2 with AD7420
{
public:
    explicit PmodTmp2(const uint8_t& adp, const uint8_t& dvc = 0x4b, const bool& res = true);  ///< device [0x48-0x4b]
    virtual ~PmodTmp2();

    bool getTemperatureInt(int16_t& t);  ///< temperature signed integer with 0.0625 LSB precision
    bool getTemperatureFloat(float& t);  ///< temperature float

    bool getResolution() {return highResolution;}  ///< get resolution, 0=13 bits, 1=16bits
    bool setResolution(const bool& r);  ///< set resolution, 0=13 bits, 1=16bits

private:
    PmodTmp2(const PmodTmp2& orig);
    PmodTmp2& operator=(const PmodTmp2& orig);

    bool getConfiguration(uint8_t& c);
    bool setConfiguration(const uint8_t& c);

    OSEF::SMBusDevice device;
    bool highResolution;
};
}  // namespace PMOD

#endif /* PMODTMP2_H */
