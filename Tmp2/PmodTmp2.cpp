#include "PmodTmp2.h"
#include "Debug.h"

PMOD::PmodTmp2::PmodTmp2(const uint8_t& adp, const uint8_t& dvc, const bool&  res)
    : device(adp, dvc)
    , highResolution(false)
{
    if((device.getDevice() < PmodTmp2DeviceMin) || (device.getDevice() > PmodTmp2DeviceMax))
    {
        _DERR("Pmod TMP2 supports only device numbers in [0x48-0x4b] not 0x" << std::hex << +device.getDevice() << std::dec);
    }

    if(!setResolution(res))
    {
        _DERR("failed to initialize resolution");
    }
}

PMOD::PmodTmp2::~PmodTmp2() {}

bool PMOD::PmodTmp2::getConfiguration(uint8_t& c)
{
    const bool ret = device.readByte(0x03, c);
    return ret;
}

bool PMOD::PmodTmp2::setConfiguration(const uint8_t& c)
{
    const bool ret = device.writeByte(0x03, c);
    return ret;
}

bool PMOD::PmodTmp2::setResolution(const bool& r)
{
    bool ret = false;

    uint8_t c = 0;
    if(getConfiguration(c))
    {
        if(r)
        {
            c |= static_cast<uint8_t>(0x80);
        }
        else
        {
            c &= static_cast<uint8_t>(~static_cast<uint8_t>(0x80));
        }

        if(setConfiguration(c))
        {
            if(getConfiguration(c))
            {
                if(c&static_cast<uint8_t>(0x80))
                {
                    highResolution = true;
                }

                if(highResolution == r)
                {
                    ret = true;
                }
            }
        }
    }

    return ret;
}

bool PMOD::PmodTmp2::getTemperatureInt(int16_t &t)
{
    bool ret = false;

    uint16_t dpt = 0;
    if(device.readWordSwap(0x00, dpt))
    {
        // remove flags if 13 bits resolution is configured
        if(!highResolution)
        {
            dpt &= static_cast<uint16_t>(0xfff8);
        }

        if(dpt >= static_cast<uint16_t>(0x8000))  // two-complements 16 bits negative values
        {
            t = dpt - 65536;
        }
        else
        {
            t = dpt;
        }

        ret = true;
    }
    else
    {
        _DERR("read temperature 2 bytes block error");
    }

    return ret;
}

bool PMOD::PmodTmp2::getTemperatureFloat(float &t)
{
    bool ret = false;

    int16_t i = 0;

    if(getTemperatureInt(i))
    {
        t = i * 0.0078125;  // ADT7420 precision is 1/128 = 0.0078125
        ret = true;
    }

    return ret;
}
