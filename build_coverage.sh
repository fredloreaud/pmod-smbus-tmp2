#!/bin/bash

mkdir -p build/Coverage
cd build/Coverage
cmake -DCMAKE_BUILD_TYPE=Coverage ../..
make
