#include "SignalHandler.h"
#include "PmodTmp2.h"
#include "TimeOut.h"

#include <cstdlib>
#include <unistd.h> // sleep
#include <iostream> // cout
#include <iomanip> // setw

bool getI2cAdapterDeviceResolution(int argc, char** argv, uint8_t& adapter, uint8_t& device, bool& resolution)
{
    bool ret = true;    
    int32_t opt = -1;
    uint64_t tmp = 0;

    if(argc>1)
    {  
	/*use function getopt to get the arguments with option."hu:p:s:v" indicate 
	that option h,v are the options without arguments while u,p,s are the
	options with arguments*/
	while((opt=getopt(argc,argv,"a:d:r:"))!=-1)
	{
	    switch(opt)
	    {
		case 'a':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if( (tmp!=0) && (tmp<=0xffUL) )
                    {
                        adapter = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
		case 'd':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffUL)
                    {
                        device = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
                case 'r':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    switch(tmp)
                    {
                        case 0:
                            resolution = false;
                            break;
                        case 1:
                            resolution = true;
                            break;
                        default:
                            ret = false;
                            break;
                    }
                    break;
		default:
                    ret = false;
                    break;
	    }
	}
    }
    
    if(!ret)
    {
        std::cout<<"Usage:   "<<*argv<<" [-option] [argument]"<<std::endl;
        std::cout<<"option:  "<<std::endl;
        std::cout<<"         "<<"-a  I2C adapter [1-255]"<<std::endl;
        std::cout<<"         "<<"-d  I2C device [0-255]"<<std::endl;
        std::cout<<"         "<<"-r  resolution [0-1]"<<std::endl;
    }
    
    return ret;
}

int main(int argc, char** argv)
{
    int32_t ret = -1;
    
    OSEF::SignalHandler signal(SIGINT, SIGTERM);
    
    uint8_t adapter = 1;
    uint8_t device = 0x4b; // Pmod Tmp2 default value
    bool resolution = true;
    if(getI2cAdapterDeviceResolution(argc, argv, adapter, device, resolution))
    {
        PMOD::PmodTmp2 pmod(adapter, device, resolution);
        float pt = 0.0;
        float ct = 0.0;
        
        if(resolution)
        {
            std::cout<<std::setprecision(7)<<std::fixed;
        }
        else
        {
            std::cout<<std::setprecision(4)<<std::fixed;
        }

        do
        {
            if(pmod.getTemperatureFloat(ct))
            {
                if(ct!=pt)
                {
                    pt = ct;
                    std::cout<<ct<<std::endl;
                }
                ret = 0;
            }
            else
            {
                ret = -1;
            }

            OSEF::sleepms(240); // typical conversion period
        }while(!signal.signalReceived() && (ret==0));
    }

    return ret;
}

