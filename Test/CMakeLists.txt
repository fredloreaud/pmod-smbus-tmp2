cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -Werror")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
set(CMAKE_CXX_FLAGS_COVERAGE "${CMAKE_CXX_FLAGS_COVERAGE} --coverage")

add_subdirectory(".." "${CMAKE_CURRENT_BINARY_DIR}/Pmod_Tmp2")
link_directories(${CMAKE_CURRENT_BINARY_DIR}/Pmod_Tmp2)

add_subdirectory("../osef-smbus" "${CMAKE_CURRENT_BINARY_DIR}/OSEF_SMBus")
link_directories(${CMAKE_CURRENT_BINARY_DIR}/OSEF_SMBus)

add_subdirectory("../osef-smbus/osef-posix" "${CMAKE_CURRENT_BINARY_DIR}/OSEF_POSIX")
link_directories(${CMAKE_CURRENT_BINARY_DIR}/OSEF_POSIX)

project(OSEF_Tmp2_Test)

add_executable(OSEF_Tmp2_Test ./main.cpp
)

target_link_libraries(OSEF_Tmp2_Test
  Pmod_Tmp2
  OSEF_SMBus
  OSEF_POSIX
  i2c
  pthread
)

include_directories(
  ../osef-smbus/osef-posix/Signal
  ../Tmp2
  ../osef-smbus/SMBus
  ../osef-smbus/osef-posix/Mutex
  ../osef-smbus/osef-posix/Time
)
